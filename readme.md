# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is used for Assignment A2.2.
The application is built on a RPC (Remote Procedure Call) architecture, that enables the client to perform challanging
computations, by using the more powerful server machine.   

The application is used to calculate the yearly tax for a car, given its engine capacity, and the selling price,
based on the manufacturing year and purchasing price.  

### How do I get set up? ###
  
This application does not require a remote or local server (e.g Tomcat), a database server or maven.  
  
To use it, follow the next steps:
	1. download the source code  
	2. import the project into an IDE (Eclipse or  IntelliJ)  
	3. run the main function from the class 'Server', which is found in the module also called 'server'  
	4. after the message 'SERVER STARTED' is printed out in the console, go to the module 'client', and run the
	main method form class GUI.  
	5. a graphical user interface opens  
  	
The two functions are run as following:  
1. Tax Computation:  
	1.1 write the engine capacity in the appropriate text field.  
	1.2 press the button called: 'Calculate Tax'.  
	1.3 a result will appear in the text box bellow.  
  	
2. Selling Price:  
	2.1 write the year of manufactoring and the purchasing price in the appropriate text fields.  
	2.2 press the button called 'Calculate Selling Price'.  
	2.3 a result will appear in the text box bellow.  
  
  
### Who do I talk to? ###

Repo owner: Anda Moldovan