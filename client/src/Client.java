import interfaces.SellingPriceService;
import interfaces.TaxService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
    public static void main(String[] args) throws RemoteException, NotBoundException {

        Registry registry = LocateRegistry.getRegistry();


        TaxService taxService = (TaxService) registry.lookup("interfaces.TaxService");

        //System.out.println("TAX: " + taxService.calculateTax(2800));

        SellingPriceService sellingPriceService = (SellingPriceService) registry.lookup("interfaces.SellingPriceService");

        //System.out.println("SELLING PRICE: " + sellingPriceService.calculateSellingPrice(10000, 2014));
    }
}
