import entities.Car;
import interfaces.SellingPriceService;
import interfaces.TaxService;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class GUI extends Application {

    Registry registry;
    TaxService taxService;
    SellingPriceService sellingPriceService;

    public GUI() throws RemoteException, NotBoundException {
        registry = LocateRegistry.getRegistry();
        taxService = (TaxService) registry.lookup("interfaces.TaxService");
        sellingPriceService = (SellingPriceService) registry.lookup("interfaces.SellingPriceService");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
//        Group root = new Group();
//        Scene scene = new Scene(root, 400, 400);
//        primaryStage.setScene(scene);

        GridPane gridPane = new GridPane();

        Label yearLbl = new Label("Year");
        TextField yearText = new TextField();
        gridPane.add(yearLbl, 0, 0);
        gridPane.setMargin(yearLbl, new Insets(5, 5, 5, 5));
        gridPane.add(yearText, 2, 0);
        gridPane.setMargin(yearText, new Insets(5, 5, 5, 5));

        Label engineLbl = new Label("Engine Size");
        TextField engineText = new TextField();
        gridPane.add(engineLbl, 0, 2);
        gridPane.setMargin(engineLbl, new Insets(5, 5, 5, 5));
        gridPane.add(engineText, 2, 2);
        gridPane.setMargin(engineLbl, new Insets(5, 5, 5, 5));


        Label priceLbl = new Label("Price");
        TextField priceText = new TextField();
        gridPane.add(priceLbl, 0, 4);
        gridPane.setMargin(priceLbl, new Insets(5, 5, 5, 5));
        gridPane.add(priceText, 2, 4);
        gridPane.setMargin(priceLbl, new Insets(5, 5, 5, 5));


        Button taxBtn = new Button("Calculate Tax");
        gridPane.add(taxBtn, 2, 6);
        gridPane.setMargin(taxBtn, new Insets(5, 5, 5, 5));

        Button priceBtn = new Button("Calculate Selling Price");
        gridPane.add(priceBtn, 3, 6);
        gridPane.setMargin(priceBtn, new Insets(5, 5, 5, 5));

        Label resultLbl = new Label("Result");
        TextField resultText = new TextField();
        gridPane.add(resultLbl, 0, 8);
        gridPane.setMargin(resultLbl, new Insets(5, 5, 5, 5));
        gridPane.add(resultText, 2, 8, 1, 2);
        gridPane.setMargin(resultText, new Insets(5, 5, 5, 5));
        resultText.setEditable(false);

        taxBtn.setOnAction(e -> {
            try {
                if ((!engineText.getText().equals(""))) {
                    hangleCalculateTaxEvent(engineText, resultText);
                } else {
                    openPopUp(primaryStage, "Engine Size field not completed");
                }
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });

        priceBtn.setOnAction(e -> {
            try {
                if(!yearText.getText().equals("") && !priceText.getText().equals("")){
                    hangleCalculateSellingPriceEvent(yearText, priceText, resultText);
                }else{
                    openPopUp(primaryStage, "Year or Price fields not completed");
                }
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });

        Scene scene = new Scene(gridPane, 400, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void hangleCalculateSellingPriceEvent(TextField yeadText, TextField priceText, TextField resultText) throws RemoteException{
        Car car = new Car();
        car.setPrice(Double.parseDouble(priceText.getText()));
        car.setYear(Integer.parseInt(yeadText.getText()));

        resultText.setText("Selling price is: " + sellingPriceService.calculateSellingPrice(car));

        System.out.println(sellingPriceService.calculateSellingPrice(car));

    }

    private void hangleCalculateTaxEvent(TextField engineSize, TextField resultText) throws RemoteException {
        Car car = new Car();
        car.setEngineSize(Integer.parseInt(engineSize.getText()));

        resultText.setText("Tax is: " + taxService.calculateTax(car));

        System.out.println(taxService.calculateTax(car));
    }


    public static void main(String args[]){
        launch(args);
    }

    private void openPopUp(Stage primaryStage, String msg){

        final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox vbox = new VBox(20);
        vbox.getChildren().add(new Text(msg));
        Scene dialogScene = new Scene(vbox, 200, 50);
        dialog.setScene(dialogScene);
        dialog.show();

    }
}
