package interfaces;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SellingPriceService extends Remote {

    double calculateSellingPrice(Car car) throws RemoteException;
}
