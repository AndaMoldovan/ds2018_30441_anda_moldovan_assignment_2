package interfaces;

import entities.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TaxService extends Remote {

    double calculateTax(Car car) throws RemoteException;
}
