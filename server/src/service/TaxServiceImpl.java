package service;

import entities.Car;
import interfaces.TaxService;

public class TaxServiceImpl implements TaxService {
    @Override
    public double calculateTax(Car car) {
        int sum = 0;

        if(car.getEngineSize() <= 1600) sum = 8;
        else if(car.getEngineSize() > 1600 && car.getEngineSize() <= 2000) sum = 18;
        else if(car.getEngineSize() > 2000 && car.getEngineSize() <= 2600) sum = 72;
        else if(car.getEngineSize() > 2600 && car.getEngineSize() <= 3000) sum = 144;
        else if(car.getEngineSize() > 3001) sum = 290;

        return sum;
    }
}
