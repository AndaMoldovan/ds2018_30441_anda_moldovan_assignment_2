package service;

import entities.Car;
import interfaces.SellingPriceService;

import java.rmi.RemoteException;

public class SellingPriceServiceImpl implements SellingPriceService {
    @Override
    public double calculateSellingPrice(Car car) throws RemoteException {

        return ((2018 - car.getYear()) < 7) ? (car.getPrice() - (car.getPrice() / 7) * (2018 - car.getYear())) : 0;
    }
}
