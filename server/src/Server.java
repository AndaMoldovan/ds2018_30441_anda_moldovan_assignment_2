import interfaces.SellingPriceService;
import interfaces.TaxService;
import service.SellingPriceServiceImpl;
import service.TaxServiceImpl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public static void main(String[] args) throws RemoteException {


        TaxService taxService = new TaxServiceImpl();
        TaxService taxStub = (TaxService) UnicastRemoteObject.exportObject(taxService, 0);

        SellingPriceService sellingService = new SellingPriceServiceImpl();
        SellingPriceService sellingStub = (SellingPriceService) UnicastRemoteObject.exportObject(sellingService, 0);

        Registry registry = LocateRegistry.createRegistry(1099);
        registry.rebind("interfaces.TaxService", taxStub);
        registry.rebind("interfaces.SellingPriceService", sellingStub);

        System.out.println("SERVER STARTED\n");
    }
}
